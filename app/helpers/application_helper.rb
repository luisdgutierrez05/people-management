# frozen_string_literal: true
# Application helper
module ApplicationHelper
  # Flash messages classes
  BOOTSTRAP_FLASH_MSG = {
    success: 'alert-success',
    error:   'alert-danger',
    alert:   'alert-warning',
    notice:  'alert-success',
    info:    'alert-info'
  }.freeze

  def alert_class_for(flash_type)
    BOOTSTRAP_FLASH_MSG[flash_type.to_sym]
  end

  def error_messages(resource)
    return '' if resource.errors.empty?
    messages = resource.errors.full_messages.map{ |msg| content_tag(:li, msg)}.join
    html = <<-HTML
    <div class="alert alert-danger media fade in"><ul>#{messages}</ul></div>
    HTML
    html.html_safe
  end
end
