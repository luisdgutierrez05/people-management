# frozen_string_literal: true

# People controller class
class PeopleController < ApplicationController
  before_action :current_person, only: [:edit, :update, :show, :destroy]

  def index
    @people = Person.all.order(first_name: :asc, last_name: :asc)
                    .paginate(page: params[:page], per_page: 6)
                    .decorate
  end

  def new
    @person = Person.new
  end

  def create
    @person = Person.new(person_params)
    if @person.save
      message = { success: I18n.t('controllers.people.create') }
      redirect_to person_path(@person), flash: message
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @person.update(person_params)
      message = { notice: I18n.t('controllers.people.update') }
      redirect_to person_path(@person), flash: message
    else
      render :edit
    end
  end

  def destroy
    deleted_person = { full_name: @person.full_name }
    @person.destroy
    Resque.enqueue(PersonNotificator, :delete, nil, deleted_person)
    message = { success: I18n.t('controllers.people.destroy') }
    redirect_to people_path, flash: message
  end

  private

  def current_person
    @person ||= PersonDecorator.find(params[:id])
  end

  def person_params
    params.require(:person).permit(:first_name, :last_name, :email, :job, :bio,
                                   :gender, :birthdate, :picture)
  end
end
