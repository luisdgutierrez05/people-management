# frozen_string_literal: true

# Pages controller class
class PagesController < ApplicationController
  def about
  end
end
