# frozen_string_literal: true
# Cannot accept futures dates validator
class CannotAcceptFuturesDatesValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value > Date.today
      record.errors[attribute] << I18n.t('activerecord.errors.models.person.attributes.birthdate.cannot_accept_futures_dates')
    end
  end
end
