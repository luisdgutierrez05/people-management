# frozen_string_literal: true
# Person Notificator Job
class PersonNotificator
  @queue = :notificator
  def self.perform(notification_type, person_id, deleted_person)
    case notification_type
    when 'create'
      person = Person.find(person_id).decorate
      Person.people_to_notify(person.id).each do |recipient|
        PersonMailer.notify_create(person, recipient).deliver_now
      end
    when 'delete'
      Person.all.each do |recipient|
        PersonMailer.notify_delete(deleted_person, recipient).deliver_now
      end
    end
  end
end
