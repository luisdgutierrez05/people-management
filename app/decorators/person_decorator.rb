# frozen_string_literal: true
# Person Decorator
class PersonDecorator < ApplicationDecorator
  delegate_all
  decorates_finders

  def full_name
    if object.first_name.blank? && object.last_name.blank?
      'No name provided'
    else
      "#{object.first_name} #{object.last_name}".strip
    end
  end

  def job_titleize
    object.job.titleize
  end

  def age
    now = Date.today
    now.year - object.birthdate.year - (object.birthdate.change(year: now.year) > now ? 1 : 0)
  end

  def birthdate_formatted
    object.birthdate.strftime('%B %d, %Y')
  end

  def gender_titleize
    object.gender.titleize
  end

  def gender_icon_name
    object.gender == Person::GENDER[0] ? 'male' : 'female'
  end

  def picture
    if object.picture?
      object.picture
    else
      Person::DEFAULT_PICTURE
    end
  end
end
