# frozen_string_literal: true
# Person mailer
class PersonMailer < ApplicationMailer
  def notify_create(person, recipient)
    @person = person
    @recipient = recipient.decorate
    subject = I18n.t('mailers.person_mailer.notify_create.subject')
    mail(to: @recipient.email, subject: subject)
  end

  def notify_delete(person, recipient)
    @person = person
    @recipient = recipient.decorate
    subject = I18n.t('mailers.person_mailer.notify_delete.subject')
    mail(to: @recipient.email, subject: subject)
  end
end
