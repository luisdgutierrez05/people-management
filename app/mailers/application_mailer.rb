# frozen_string_literal: true
# Application mailer base
class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@peoplemanagement.com'
  layout 'mailer'
end
