# frozen_string_literal: true
# == Schema Information
#
# Table name: people
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  last_name  :string(255)
#  email      :string(254)
#  job        :string(255)
#  bio        :text(65535)
#  gender     :string(255)
#  birthdate  :date
#  picture    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Person Model
class Person < ActiveRecord::Base
  # Constants
  GENDER = %w(male female).freeze
  EMAIL_REGEX = /\A[^@]+@[^@]+\z/
  DEFAULT_PICTURE = ActionController::Base.helpers.image_path('default-picture.png')

  # Validations
  validates :first_name, presence: true
  validates :first_name, length: { maximum: 75 }

  validates :last_name, presence: true
  validates :last_name, length: { maximum: 75 }

  validates :email, presence: true
  validates :email, length: { maximum: 254 }
  validates :email, uniqueness: true
  validates :email, format: { with: EMAIL_REGEX }

  validates :job, length: { maximum: 75 }, allow_nil: true

  validates :gender, presence: true
  validates :gender, inclusion: GENDER

  validates :birthdate, presence: true
  validates :birthdate, cannot_accept_futures_dates: true, if: 'birthdate.present?'

  # Scopes
  scope :people_to_notify, -> (id) { where.not(id: id) }

  # Callbacks
  after_create :notify_create_to_all

  private

  def notify_create_to_all
    Resque.enqueue(PersonNotificator, :create, id, nil)
  end
end
