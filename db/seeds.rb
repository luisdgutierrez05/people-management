# People
20.times do
  person = Person.new(
    first_name: Faker::Name.first_name,
    last_name:  Faker::Name.last_name,
    email:      Faker::Internet.email,
    job:        Faker::Company.profession,
    bio:         Faker::Hipster.paragraph,
    gender:     Person::GENDER.sample,
    birthdate:  Faker::Date.birthday(18, 65),
    picture:    Faker::Avatar.image('', '200x200', 'jpg')
  )
  person.save!
end
