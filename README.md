# People Management System

> People Management System (PMS) is a RoR application to manage people through
> CRUD (basics functions: list, create, update and delete persons) operations.
> Also have a notification system that allows notify all people via email when
> some person has been created or deleted.


Delivery Date: ```Friday 23 Sep 2016 08:00PM UTC-05:00```

Developed by: ```Luis D. Gutiérrez```

System Requirements
-------------------

* Rails 4.2.7.1
* Ruby 2.2.4
* Mysql
* Redis
* Resque

Installing Dependencies
-----------------------

```
  $ brew install mysql
  $ brew install redis
```

Setting up Database
-------------------

There is a sample database configuration file at ``config/database.yml.example`` you would need to create a new ``config/database.yml`` and configure your [Mysql](https://www.mysql.com/) configuration.

After configure your database server in rails:

* ``$ rake db:setup``

Env Vars Descriptions
------------------------

``.env.example`` is the example file for a required file that ``foreman`` needs to setup the env vars.

```
  $ cp .env.example .env
```

By default People Management needs this env vars to be defined in ``.env`` file:
* ``PORT`` is the port number where is going to be running the application in localhost
* ``RACK_ENV`` sets the rack development name, by default in the ``.env.example`` is set to ``development``

Running People Management
-------------------------

Before you start the application make sure you have running [Mysql](https://www.mysql.com/) and [Redis](http://redis.io/) dependencies.
```
$ brew services start mysql
```
This will start Mysql database.

```
  $ foreman start -f Procfile.dev
```

This will start the required services on development mode, Redis and Puma.


Testing Frameworks
------------------

* ``Minitest``

How to run Tests
----------------
```
  $ rake test
```

Bootstrap Themes
----------------
[Round About](https://startbootstrap.com/template-overviews/round-about/)
[Freelancer](http://startbootstrap.com/template-overviews/freelancer/)
