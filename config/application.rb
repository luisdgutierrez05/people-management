require File.expand_path('../boot', __FILE__)
require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# People Management module
module PeopleManagement
  # Application base class
  class Application < Rails::Application
    config.autoload_paths += %W(#{config.root}/lib)

    # Custom validators path
    config.autoload_paths += %W["#{config.root}/app/validators/"]

    # Decorators
    config.autoload_paths += %W["#{config.root}/app/decorators/"]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.generators do |g|
      g.test_framework :minitest, spec: true, fixture: false
    end
  end
end
