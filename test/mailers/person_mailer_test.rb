require 'test_helper'

describe PersonMailer do
  before do
    FactoryGirl.create_list(:person, 2)
    @recipient = Person.first.decorate
  end

  it 'sends a notification when person is created' do
    new_person = FactoryGirl.create(:person).decorate
    email = PersonMailer.notify_create(new_person, @recipient)
    assert_emails 1 do
      email.deliver_now
    end
    assert_equal(email.subject, I18n.t('mailers.person_mailer.notify_create.subject'))
    assert_equal(email.from, ['no-reply@peoplemanagement.com'])
    assert_equal(email.to, [@recipient.email])
  end

  it 'sends a notification when an existing person is deleted' do
    deleted_person = { full_name: Person.last.decorate.full_name }
    Person.last.destroy
    email = PersonMailer.notify_delete(deleted_person, @recipient)
    assert_emails 1 do
      email.deliver_now
    end
    assert_equal(email.subject, I18n.t('mailers.person_mailer.notify_delete.subject'))
    assert_equal(email.from, ['no-reply@peoplemanagement.com'])
    assert_equal(email.to, [@recipient.email])
  end
end
