require 'test_helper'

class BirthdateValidatable
  include ActiveModel::Validations
  attr_accessor :birthdate

  validates :birthdate, cannot_accept_futures_dates: true
end

class CannotAcceptFuturesDatesValidatorTest < ActiveSupport::TestCase
  valid_birthdate = (Date.today - 20.years)
  invalid_birthdates = [(Date.today + 1.day), (Date.today + 1.year)]

  def obj
    @obj ||= BirthdateValidatable.new
  end

  it 'should invalidate birthdate' do
    invalid_birthdates.each do |birthdate|
      obj.birthdate = birthdate
      assert_not obj.valid?
    end
  end

  test 'should add error for invalid birthdate' do
    invalid_birthdates.each do |birthdate|
      obj.birthdate = birthdate
      obj.valid?
      assert_equal I18n.t('activerecord.errors.models.person.attributes.birthdate.cannot_accept_futures_dates'), obj.errors[:birthdate][0], 'cannot accept future dates'
    end
  end

  it 'should validate birthdate' do
    obj.birthdate = valid_birthdate
    assert obj.valid?
  end

  it 'should add no error for valid birthdate' do
    obj.birthdate = valid_birthdate
    assert obj.errors[:birthdate].blank?
  end
end
