# == Schema Information
#
# Table name: people
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  last_name  :string(255)
#  email      :string(254)
#  job        :string(255)
#  bio        :text(65535)
#  gender     :string(255)
#  birthdate  :date
#  picture    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  # Validations
  should validate_presence_of(:first_name)
  should validate_length_of(:first_name).is_at_most(75)

  should validate_presence_of(:last_name)
  should validate_length_of(:last_name).is_at_most(75)

  should validate_presence_of(:email)
  should validate_length_of(:email).is_at_most(254)
  should validate_uniqueness_of(:email)
  should allow_value('hello@company.com').for(:email)
  should_not allow_value('@hello').for(:email)

  should validate_length_of(:job).is_at_most(75)
  should allow_value(nil).for(:job)

  should validate_presence_of(:gender)
  should validate_inclusion_of(:gender).in_array(Person::GENDER)

  should validate_presence_of(:birthdate)
  should allow_value((Date.today - 18.years)).for(:birthdate)
  should_not allow_value((Date.today + 1.day)).for(:birthdate)
end
