require 'test_helper'

module PeopleController
  class RoutesTest < ActionController::TestCase
    def test_routes
      assert_routing '/', controller: 'people', action: 'index'
      assert_routing '/people/new', controller: 'people', action: 'new'
      assert_routing({ method: 'post', path: '/people' }, controller: 'people',
                                                          action: 'create')
      assert_routing '/people/1', controller: 'people', action: 'show', id: '1'
      assert_routing '/people/1/edit', controller: 'people', action: 'edit',
                                       id: '1'
      assert_routing({ method: 'patch', path: '/people/1' },
                     controller: 'people', action: 'update', id: '1')
      assert_routing({ method: 'delete', path: '/people/1' },
                     controller: 'people', action: 'destroy', id: '1')
    end
  end
end
