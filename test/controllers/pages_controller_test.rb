require 'test_helper'

describe PagesController do
  describe 'GET #about' do
    before do
      get :about
    end

    it 'responds with success' do
      must_respond_with :success
    end
    it 'renders pages/about' do
      must_render_template 'pages/about'
    end
  end
end
