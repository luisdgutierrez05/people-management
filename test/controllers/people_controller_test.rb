require 'test_helper'

describe PeopleController do
  setup do
    @person = FactoryGirl.create(:person)
  end

  describe 'GET #index' do
    before do
      get :index
    end

    it 'responds with success' do
      must_respond_with :success
      assert_not_nil assigns(:people)
    end
    it 'renders people/index' do
      must_render_template 'people/index'
    end
  end

  describe 'GET #new' do
    before do
      get :new
    end

    it 'responds with success' do
      assert_response :success
      assert_not_nil assigns(:person)
    end
    it 'renders people/new' do
      must_render_template 'people/new'
    end
  end

  describe 'POST #create' do
    it 'creates the person record' do
      person_params = FactoryGirl.attributes_for(:person)
      assert_difference('Person.count') do
        post :create, person: person_params
      end
      assert_redirected_to person_path(Person.last)
      assert_equal I18n.t('controllers.people.create'), flash[:success]
    end
  end

  describe 'GET #show' do
    before do
      get :show, id: @person.id
    end

    it 'responds with success' do
      assert_response :success
      assert_not_nil assigns(:person)
    end
    it 'renders people/show' do
      must_render_template 'people/show'
    end
  end

  describe 'GET #edit' do
    before do
      get :edit, id: @person.id
    end

    it 'responds with success' do
      assert_response :success
      assert_not_nil assigns(:person)
    end
    it 'renders people/edit' do
      must_render_template 'people/edit'
    end
  end

  describe 'PATCH #update' do
    it 'updates the person record' do
      person_params = { job: 'web developer', bio: 'bio information' }
      patch :update, id: @person.id, person: person_params

      assert_redirected_to person_path(@person)
      assert_equal I18n.t('controllers.people.update'), flash[:notice]

      @person.reload
      assert_equal 'web developer', @person.job
      assert_equal 'bio information', @person.bio
    end
  end

  describe 'DELETE #destroy' do
    it 'deletes the person record' do
      assert_difference('Person.count', -1) do
        delete :destroy, id: @person.id
      end

      assert_redirected_to people_path
      assert_equal I18n.t('controllers.people.destroy'), flash[:success]
    end
  end
end
